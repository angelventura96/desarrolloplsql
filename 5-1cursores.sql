DECLARE
    CURSOR cur_depts IS
    SELECT  department_id, 
            department_name 
    FROM departments;
            v_department_id departments.department_id %TYPE;
            v_department_name departments.department_name %TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO 
            v_department_id, 
            v_department_name;
        EXIT WHEN cur_depts %NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_department_id||' '||v_department_name);
    END LOOP;
    CLOSE cur_depts;
END;

DECLARE
    CURSOR cur_depts2 IS
    SELECT  department_id, 
            department_name,
            location_id
    FROM departments;
            v_department_id departments.department_id %TYPE;
            v_department_name departments.department_name %TYPE;
            v_location departments.location_id %TYPE;
BEGIN
    OPEN cur_depts2;
    LOOP
        FETCH cur_depts2 INTO 
            v_department_id, 
            v_department_name,
            v_location;
        EXIT WHEN cur_depts2 %NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_department_id||' '||v_department_name||' ' ||v_location);
    END LOOP;
    CLOSE cur_depts2;
END;


DECLARE
    CURSOR cur_emps3 IS
    SELECT *
    FROM employees
    WHERE department_id = 30;
    v_emp_record cur_emps3%ROWTYPE;
BEGIN
    OPEN cur_emps3;
    LOOP
    FETCH cur_emps3
    INTO v_emp_record;
    EXIT WHEN cur_emps3%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' - '
    || v_emp_record.last_name);
    END LOOP;
    CLOSE cur_emps3;
END;


DECLARE
    CURSOR cur_emps4 IS
    SELECT employee_id, last_name
    FROM employees
    WHERE department_id = 50;
BEGIN
    FOR v_emp_record IN cur_emps4 LOOP
        DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' '
        || v_emp_record.last_name);
    END LOOP;
END;


DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id, 
        department_name
    FROM
        departments
    ORDER BY department_id;
BEGIN
    FOR
        v_dept_record IN cur_depts LOOP
        DBMS_OUTPUT.PUT_LINE(
        v_dept_record.department_id||' ' || v_dept_record.department_name);
    END LOOP;
END;

DECLARE
    CURSOR cur_countries (p_region_id NUMBER, p_country_id CHAR) IS
    SELECT  country_id, country_name
    FROM countries
    WHERE region_id = p_region_id OR country_id = 'BR';
  
BEGIN
    FOR v_country_record IN cur_countries(145,'BR')LOOP
        DBMS_OUTPUT.PUT_LINE(v_country_record.country_id||'----'||v_country_record. country_name);
    END LOOP;
END;

DECLARE
    CURSOR cur_emps IS
    SELECT employee_id, salary 
    FROM employees
    WHERE salary <=20000 FOR UPDATE NOWAIT;
    v_emp_rec cur_emps%ROWTYPE;
BEGIN
    OPEN cur_emps;
        LOOP FETCH cur_emps INTO v_emp_rec;
            EXIT WHEN cur_emps%NOTFOUND;
            UPDATE employees
                SET salary = v_emp_rec.salary*1.1
            WHERE CURRENT OF cur_emps;
        END LOOP;
    CLOSE cur_emps;
END;


DECLARE
    CURSOR cur_loc IS SELECT * FROM locations;
    CURSOR cur_dept (p_locid NUMBER) IS
        SELECT * FROM departments WHERE location_id = p_locid;
BEGIN
FOR v_locrec IN cur_loc
    LOOP
        DBMS_OUTPUT.PUT_LINE(v_locrec.city);
        FOR v_deptrec IN cur_dept (v_locrec.location_id)
            LOOP
            DBMS_OUTPUT.PUT_LINE(v_deptrec.department_name);
            END LOOP;
    END LOOP;
END;



