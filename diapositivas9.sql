CREATE OR REPLACE FUNCTION get_sal
(p_id IN employees.employee_id%TYPE)
RETURN NUMBER IS
v_sal employees.salary%TYPE:= 0;
BEGIN
    SELECT salary
    INTO v_sal
    FROM employees
    WHERE employee_id = p_id;
    RETURN v_sal;
END get_sal;
--EJECUTAMOS LA FUNCION GETSAL
DECLARE
    v_sal employees.salary%type;
BEGIN
    v_sal:= get_sal(100); 
    DBMS_OUTPUT.put_line('salario'||v_sal);
END;



SELECT employee_id, job_id, get_sal(employee_id) FROM employees;



SELECT 'patito' FROM departments WHERE department_id = 100;

CREATE OR REPLACE FUNCTION valid_dept(p_dept_no departments.department_id%TYPE)
RETURN BOOLEAN IS v_valid VARCHAR2(1);
BEGIN
    SELECT 'x' INTO v_valid
    FROM departments
    WHERE department_id = p_dept_no;
    RETURN(true);
EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN(false);
    WHEN OTHERS THEN NULL;
END;

BEGIN
    IF valid_dept(1000) THEN  
        DBMS_OUTPUT.put_line('TRUE') ;
    END IF;
END;

BEGIN
    IF valid_dept(1000) THEN
        dbms_output.put_line('TRUE');
    ELSE
        dbms_output.put_line('FALSE');
        DBMS_OUTPUT.PUT_LINE(USER);
    END IF;
END;



SELECT job_id, SYSDATE-hire_date FROM employees;



CREATE OR REPLACE FUNCTION tax(p_value IN NUMBER)
RETURN NUMBER IS
BEGIN
RETURN (p_value * 0.08);
END tax;

SELECT employee_id, last_name, salary, tax(salary) "Impuesto"
FROM employees
WHERE department_id= 50;



SELECT COUNT(*) FROM DICT

SELECT table_name FROM DICT

SELECT COUNT(*) from DICT where table_name like 'USER%';

SELECT COUNT(*) from DICT where table_name like 'ALL%';

SELECT COUNT(*) from DICT where table_name not like 'USER%' AND table_name not like 'ALL%';




