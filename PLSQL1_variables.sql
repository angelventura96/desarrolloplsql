CREATE OR REPLACE PROCEDURE print_date IS v_date VARCHAR2(30);
BEGIN
SELECT TO_CHAR(SYSDATE,'Mon DD, YYYY')
INTO v_date
FROM DUAL;
DBMS_OUTPUT.PUT_LINE(v_date);
END;

BEGIN
PRINT_DATE;
END;

CREATE OR REPLACE FUNCTION tomorrow (p_today IN DATE)
RETURN DATE IS v_tomorrow DATE;
BEGIN
SELECT p_today + 1 INTO v_tomorrow
FROM DUAL;
RETURN v_tomorrow;
END;

SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"
FROM DUAL;

BEGIN
DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE + 1));
END;

DECLARE
v_emp_count NUMBER;
BEGIN
DBMS_OUTPUT.PUT_LINE('PL/SQL is easy so far!');
SELECT COUNT(*) INTO v_emp_count FROM employees;
DBMS_OUTPUT.PUT_LINE('There are '||v_emp_count||' rows in the
employees table');
END;

DECLARE
v_counter INTEGER := 0;
BEGIN
v_counter:= v_counter+ 1;
DBMS_OUTPUT.PUT_LINE(v_counter);
END;

DECLARE
v_myname VARCHAR2(20);
BEGIN
DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
v_myname:= 'John';
DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
END;

DECLARE
v_date VARCHAR2(30);
BEGIN
SELECT TO_CHAR(SYSDATE)INTO v_date FROM DUAL;
DBMS_OUTPUT.PUT_LINE(v_date);
END;

CREATE OR REPLACE FUNCTION num_characters(p_string IN VARCHAR2) RETURN INTEGER IS
v_num_characters INTEGER;
BEGIN
SELECT LENGTH(p_string) INTO v_num_characters
FROM DUAL;
RETURN v_num_characters;
END;

DECLARE
v_length_of_string INTEGER;
BEGIN
v_length_of_string := num_characters('Oracle Corporation');
DBMS_OUTPUT.PUT_LINE(v_length_of_string);
END;