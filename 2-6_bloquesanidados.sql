DECLARE
v_outer_variable VARCHAR2(20):='GLOBAL VARIABLE';
BEGIN
    DECLARE
    v_inner_variable VARCHAR2(20):='LOCAL VARIABLE';
    BEGIN
    DBMS_OUTPUT.PUT_LINE('bloque interno'||v_inner_variable);
    DBMS_OUTPUT.PUT_LINE('bloque interno'||v_outer_variable);
    END;
DBMS_OUTPUT.PUT_LINE('bloque externo'||v_outer_variable);
END;

