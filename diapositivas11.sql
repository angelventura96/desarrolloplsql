CREATE OR REPLACE PACKAGE curs_pkg IS
CURSOR
emp_curs IS SELECT employee_id FROM employees
ORDER BY employee_id;
PROCEDURE open_curs;
FUNCTION
fetch_n_rows(n NUMBER := 1) RETURN BOOLEAN;
PROCEDURE close_curs;
END curs_pkg;

CREATE OR REPLACE PACKAGE BODY curs_pkg IS
    PROCEDURE open_curs IS
    BEGIN
        IF NOT emp_curs%ISOPEN THEN OPEN emp_curs; END IF;
    END open_curs;
    FUNCTION fetch_n_rows(n NUMBER :=1) RETURN BOOLEAN IS
        emp_id employees.employee_id%TYPE;
        BEGIN
            FOR count IN 1 .. n LOOP
                FETCH emp_curs INTO emp_id;
                EXIT WHEN emp_curs%NOTFOUND;
                DBMS_OUTPUT.PUT_LINE('Id: ' ||(emp_id));
            END LOOP;
        RETURN emp_curs%FOUND;
    END fetch_n_rows;
    PROCEDURE close_curs IS 
    BEGIN
        IF emp_curs%ISOPEN 
            THEN CLOSE emp_curs; 
        END IF;
    END close_curs;
END curs_pkg;

DECLARE
    v_more_rows_exist BOOLEAN := TRUE;
BEGIN curs_pkg.open_curs;--1
    LOOP v_more_rows_exist := curs_pkg.fetch_n_rows(3); --2
        DBMS_OUTPUT.PUT_LINE('-------');
        EXIT WHEN NOT v_more_rows_exist;
    END LOOP;
    curs_pkg.close_curs; --3
END;





