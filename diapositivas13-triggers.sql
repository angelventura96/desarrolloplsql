Drop table log_table; --se elimina la tabla si ya existe

CREATE TABLE LOG_TABLE --definicion de la tabla
(USER_ID VARCHAR2(9) NOT NULL , 
logon_date DATE , 
CONSTRAINT LOG_TABLE_PK PRIMARY KEY (USER_ID, logon_date) ENABLE 
);

CREATE OR REPLACE TRIGGER log_sal_change_trigg -- creacion del trigger
    AFTER UPDATE OF salary ON employees -- despues de actualizar salary de employees
BEGIN
    INSERT INTO log_table (user_id, logon_date) --creara un log del usuario y fecha dela actualizacion
    VALUES (USER, SYSDATE);
    dbms_output.put_line('se actualizo un salario desde el trigger'); --salida DBMSoutput
END;

UPDATE employees -- se actualiza el salary
set salary = salary * 1.2
where employee_id = 100;

select * from log_table; -- verificar que si se ejecuto el trigger






CREATE TABLE new_emps AS
SELECT employee_id,last_name,salary,department_id
FROM employees;

CREATE TABLE new_depts AS
SELECT d.department_id,d.department_name,
sum(e.salary) dept_sal
FROM employees e, departments d
WHERE e.department_id= d.department_id
GROUP BY d.department_id,d.department_name;

CREATE VIEW emp_details AS
SELECT e.employee_id, e.last_name, e.salary,
e.department_id, d.department_name
FROM new_emps e, new_depts d
WHERE e.department_id= d.department_id;

CREATE OR REPLACE TRIGGER new_emp_dept
INSTEAD OF INSERT ON emp_details
BEGIN
INSERT INTO new_emps
VALUES (:NEW.employee_id, :NEW.last_name,
:NEW.salary, :NEW.department_id);
dbms_output.put_line('se inserto un salario desde el trigger');
UPDATE new_depts
SET dept_sal= dept_sal+ :NEW.salary
WHERE department_id= :NEW.department_id;
dbms_output.put_line('se actualizo un salario desde el trigger');
END;

INSERT INTO emp_details
VALUES (9001, 'ABBOTT', 3000, 10, 'Administration');




