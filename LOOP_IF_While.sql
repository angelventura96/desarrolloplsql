DECLARE
v_deptno copy_employees.department_id %TYPE := 50;
BEGIN
DELETE FROM copy_employees
WHERE
department_id= v_deptno;
DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' rows deleted.');
END;

CREATE TABLE results (num_rows NUMBER(4));
BEGIN
UPDATE  copy_employees
SET salary = salary + 100
WHERE job_id = 'ST_CLERK';
INSERT INTO results (num_rows)
VALUES (SQL%ROWCOUNT);
END;

DECLARE
v_counter NUMBER(2) := 1;
BEGIN
LOOP
DBMS_OUTPUT.PUT_LINE('Loop execution #' || v_counter);
v_counter:= v_counter + 1;
EXIT WHEN v_counter > 5;
END LOOP;
END;

BEGIN
FOR v_outerloop IN 1..3 LOOP
FOR v_innerloop IN REVERSE 1..5 LOOP
DBMS_OUTPUT.PUT_LINE('Outer loop is: ' ||v_outerloop ||' and inner loop is: ' ||v_innerloop);
END LOOP;
END LOOP;
END;