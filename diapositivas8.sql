CREATE OR REPLACE PROCEDURE add_dept IS
v_dept_id departments.department_id%TYPE;
v_dept_name departments.department_name%TYPE;
BEGIN
v_dept_id:= 282;
v_dept_name:= 'ST-Curriculum';
INSERT INTO departments(department_id, department_name)
VALUES(v_dept_id, v_dept_name);
DBMS_OUTPUT.PUT_LINE('Inserted '|| SQL%ROWCOUNT || ' row.');
END;

BEGIN
add_dept;
END;


CREATE OR REPLACE PROCEDURE raise_salary
(
p_id IN employees.employee_id%TYPE,
p_percent IN NUMBER)
IS
BEGIN
UPDATE
employees
SET salary = salary * (1 +
p_percent/100)
WHERE employee_id =
p_id;
END raise_salary;



CREATE OR REPLACE PROCEDURE process_employees
IS
CURSOR emp_cursor IS
SELECT employee_id
FROM
employees;
BEGIN
FOR
v_emp_rec IN emp_cursor
LOOP
raise_salary(
v_emp_rec.employee_id, 10);
END LOOP;
END process_employees;



CREATE OR REPLACE PROCEDURE query_emp(  p_id IN employees.employee_id%TYPE,
                                        p_name OUT employees.last_name%TYPE,
                                        p_salary OUT employees.salary%TYPE) IS
BEGIN
    SELECT last_name, salary INTO p_name, p_salary
    FROM employees
    WHERE employee_id = p_id;
END query_emp;


DECLARE
    a_emp_name employees.last_name%TYPE;
    a_emp_sal employees.salary%TYPE;
BEGIN
    query_emp(178,a_emp_name, a_emp_sal);
    DBMS_OUTPUT.PUT_LINE('Name: ' || a_emp_name);
    DBMS_OUTPUT.PUT_LINE('Salary: ' || a_emp_sal);
END;



