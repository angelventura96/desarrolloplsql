DECLARE
    v_country_name countries.country_name%TYPE := 'Korea, South';
    v_reg_id countries.region_id%TYPE;
BEGIN
    SELECT region_id INTO v_reg_id
        FROM countries WHERE country_name = v_country_name;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN --Si el registro no tiene datos
        DBMS_OUTPUT.PUT_LINE ('Country name, ' || v_country_name || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;



DECLARE
    v_lname employees.last_name%TYPE;
BEGIN
    SELECT last_name INTO v_lname
    FROM employees 
    WHERE job_id= 'ST_CLERK';
    DBMS_OUTPUT.PUT_LINE('The last name of the ST_CLERK is:' || v_lname);
    EXCEPTION
    WHEN TOO_MANY_ROWS 
        THEN
            DBMS_OUTPUT.PUT_LINE ('Your select statement retrievedmultiplerows. Consider using a cursor.');
END;


DECLARE
e_insert_excep EXCEPTION; --se declara la excepcion
PRAGMA EXCEPTION_INIT (e_insert_excep, -01400);
BEGIN
INSERT INTO departments(department_id, department_name)
VALUES (280, NULL);
EXCEPTION --cacha la excepcion
WHEN e_insert_excep --identifica la excepcion que definimos
THEN
DBMS_OUTPUT.PUT_LINE('INSERT FAILED'); --texto a msotrar si se cacha ese error
END;


DECLARE
    e_invalid_department EXCEPTION;
    v_name VARCHAR2(20):='Accounting';
    v_deptno NUMBER :=27;
BEGIN
    UPDATE departments
    SET department_name = v_name
    WHERE department_id = v_deptno;
    IF SQL%NOTFOUND THEN
        RAISE e_invalid_department;
    END IF;
    EXCEPTION
    WHEN e_invalid_department
        THEN DBMS_OUTPUT.PUT_LINE('No such department id.');
END;


DECLARE
    v_mgr PLS_INTEGER := 27;
    v_employee_id employees.employee_id%TYPE;
BEGIN
    SELECT employee_id INTO v_employee_id
    FROM employees
    WHERE manager_id = v_mgr;
    DBMS_OUTPUT.PUT_LINE('Employee #' || v_employee_id ||' works for manager #' || v_mgr || '.');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20201,'This manager has no employees');
        WHEN TOO_MANY_ROWS THEN
                RAISE_APPLICATION_ERROR(-20202,'Too many employees were found.');
END;



BEGIN
DECLARE
e_myexcep EXCEPTION;
BEGIN
RAISE e_myexcep;
DBMS_OUTPUT.PUT_LINE(
'Message 1');
EXCEPTION
WHEN TOO_MANY_ROWS THEN
DBMS_OUTPUT.PUT_LINE('Message 2');
END;
DBMS_OUTPUT.PUT_LINE('Message 3');
EXCEPTION
WHEN e_myexcep THEN
DBMS_OUTPUT.PUT_LINE('Message 4');
END;



DECLARE
e_myexcep EXCEPTION;
BEGIN
BEGIN
RAISE e_myexcep;
DBMS_OUTPUT.PUT_LINE(
'Message 1');
EXCEPTION
WHEN TOO_MANY_ROWS THEN
DBMS_OUTPUT.PUT_LINE('Message 2');
END;
DBMS_OUTPUT.PUT_LINE('Message 3');
EXCEPTION
WHEN e_myexcep THEN
DBMS_OUTPUT.PUT_LINE('Message 4');
END;


DECLARE
e_myexcep EXCEPTION;
BEGIN
BEGIN
RAISE e_myexcep;
DBMS_OUTPUT.PUT_LINE(
'Message 1');
EXCEPTION
WHEN TOO_MANY_ROWS THEN
DBMS_OUTPUT.PUT_LINE('Message 2');
END;
DBMS_OUTPUT.PUT_LINE('Message 3');
EXCEPTION
WHEN NO_DATA_FOUND THEN
DBMS_OUTPUT.PUT_LINE('Message 4');
END;