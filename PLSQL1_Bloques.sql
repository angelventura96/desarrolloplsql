BEGIN
DBMS_OUTPUT.PUT_LINE('PL/SQL is easy!');
END;

DECLARE
v_date DATE := SYSDATE;
BEGIN
DBMS_OUTPUT.PUT_LINE(v_date);
END;

BEGIN 
    FOR c IN (SELECT * FROM USER_TABLES) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE becaplsql'||c.TABLE_NAME||' AS SELECT * FROM HR.'||c.TABLE_NAME;
    END LOOP;
END;


DECLARE
v_first_name VARCHAR2(25);
v_last_name VARCHAR2(25);
BEGIN
SELECT first_name, last_name
INTO
v_first_name, v_last_name
FROM employees
WHERE last_name = 'Oswald';
DBMS_OUTPUT.PUT_LINE ('The employee of the month is: '||v_first_name|| ' ' || v_last_name|| '.');
EXCEPTION
WHEN TOO_MANY_ROWS THEN
DBMS_OUTPUT.PUT_LINE ('Your select statement retrieved
multiple rows. Consider using a cursor or changing
the search criteria.');
END;