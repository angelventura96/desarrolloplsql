DECLARE
v_myname VARCHAR2(20);
BEGIN
DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
v_myname:= 'John';
DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
END;

CREATE FUNCTION num_characters(p_string IN VARCHAR2) RETURN INTEGER IS
v_num_characters INTEGER;
BEGIN
SELECT LENGTH(p_string) INTO v_num_characters
FROM DUAL;
RETURN v_num_characters;
END;

DECLARE
v_length_of_string INTEGER;
BEGIN
v_length_of_string := num_characters('Oracle Corporation');
DBMS_OUTPUT.PUT_LINE(v_length_of_string);
END;

