<<f>>
DECLARE
v_nombre_padre          VARCHAR2(15):= 'Pedro';
v_fecha_nacimiento      DATE:= '15-ENERO-1994';
v_referencia            VARCHAR2(15):='hijo';
BEGIN
    <<d>>
    DECLARE
    v_nombre_hijo       VARCHAR2(15):='Julian';
    v_fecha_nacimiento  DATE:= '17-FEBRERO-2018';
    v_referencia        VARCHAR2(15):='padre';
    BEGIN
        <<md>>
        DECLARE
        v_nombre_hijo       VARCHAR2(15):='Julian';
        BEGIN
     DBMS_OUTPUT.PUT_LINE ('Nombre del Padre:'|| f.v_nombre_padre);
     DBMS_OUTPUT.PUT_LINE('Fecha de Nacimiento:' || f.v_fecha_nacimiento);
     DBMS_OUTPUT.PUT_LINE('Referencia:' || v_referencia);
     DBMS_OUTPUT.PUT_LINE('----------------------------------');
     DBMS_OUTPUT.PUT_LINE('Nombre del hijo:' || md.v_nombre_hijo);
     DBMS_OUTPUT.PUT_LINE('Fecha de Nacimiento:' || d.v_fecha_nacimiento);
     DBMS_OUTPUT.PUT_LINE('Referencia:' || v_referencia);
END;
    DBMS_OUTPUT.PUT_LINE('----------------------------------');
    DBMS_OUTPUT.PUT_LINE('Fecha de Nacimiento del Hijo:' || v_fecha_nacimiento);
END;
    DBMS_OUTPUT.PUT_LINE('----------------------------------');
    DBMS_OUTPUT.PUT_LINE('Fecha de Nacimiento del Padre:' || v_fecha_nacimiento);
END;