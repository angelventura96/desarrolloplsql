CREATE OR REPLACE PACKAGE
check_emp_pkg IS
g_max_length_of_service CONSTANT NUMBER := 100;
PROCEDURE chk_hiredate(p_date IN employees.hire_date%TYPE);
PROCEDURE
chk_dept_mgr (p_empid IN employees.employee_id%TYPE, p_mgr IN employees.manager_id%TYPE);
END

check_emp_pkg;



CREATE OR REPLACE PACKAGE manage_jobs_pkg
IS g_todays_date DATE := SYSDATE;
CURSOR jobs_curs IS
    SELECT employee_id, job_id FROM employees
    ORDER BY employee_id;
PROCEDURE update_job (p_emp_id IN employees.employee_id%TYPE);
PROCEDURE fetch_emps (p_job_id IN employees.job_id%TYPE,p_emp_id OUT employees.employee_id%TYPE);
END manage_jobs_pkg;



CREATE OR REPLACE PACKAGE BODY check_emp_pkg is
PROCEDURE chk_hiredate (p_date IN employees.hire_date%TYPE)
IS BEGIN
    IF MONTHS_BETWEEN(SYSDATE, p_date) >
        g_max_length_of_service * 12 THEN
        RAISE_APPLICATION_ERROR(-20200, 'Invalid Hiredate');
    END IF;
END chk_hiredate;
PROCEDURE chk_dept_mgr(p_empid IN employees.employee_id%TYPE, p_mgr IN employees.manager_id%TYPE)
IS BEGIN dbms_output.put('');
END chk_dept_mgr;
END check_emp_pkg;



CREATE OR REPLACE PACKAGE salary_pkg
IS g_max_sal_raiseCONSTANT NUMBER := 0.20;
PROCEDURE update_sal
(p_employee_id employees.employee_id%TYPE,
p_new_salary employees.salary%TYPE);
END salary_pkg;




CREATE OR REPLACE PACKAGE global_consts IS
mile_to_kilo CONSTANT NUMBER := 1.6093;
kilo_to_mile CONSTANT NUMBER := 0.6214;
yard_to_meter CONSTANT NUMBER := 0.9144;
meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

GRANT EXECUTE ON global_consts TO PUBLIC;

DECLARE
distance_in_miles NUMBER(5) := 5000;
distance_in_kilo NUMBER(6,2);
BEGIN
distance_in_kilo :=
distance_in_miles * global_consts.mile_to_kilo;
DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;



CREATE OR REPLACE PACKAGE our_exceptions IS
e_cons_violation EXCEPTION;
PRAGMA EXCEPTION_INIT (e_cons_violation, -2292);
e_value_too_large EXCEPTION;
PRAGMA EXCEPTION_INIT (e_value_too_large, -1438);
END our_exceptions;

GRANT EXECUTE ON our_exceptions TO PUBLIC;

CREATE TABLE excep_test(number_col NUMBER(3));

BEGIN
INSERT INTO excep_test (number_col) VALUES (12345);
EXCEPTION
WHEN our_exceptions.e_value_too_large THEN
DBMS_OUTPUT.PUT_LINE('Value too big for column data type');
END;



CREATE OR REPLACE PACKAGE taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER;
END taxes_pkg;

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER IS
v_rate NUMBER := 0.08;
BEGIN
RETURN (p_value * v_rate);
END tax;
END taxes_pkg;

SELECT taxes_pkg.tax(salary), salary, last_name
FROM employees;




CREATE OR REPLACE PROCEDURE sel_one_emp
(p_emp_id IN employees.employee_id%TYPE,
p_emprec OUT employees%ROWTYPE)
IS BEGIN
SELECT * INTO p_emprec FROM employees
WHERE employee_id = p_emp_id;
END sel_one_emp;

DECLARE
v_emprec employees%ROWTYPE;
BEGIN
sel_one_emp(100, v_emprec);
dbms_output.put_line(v_emprec.last_name);
END;




