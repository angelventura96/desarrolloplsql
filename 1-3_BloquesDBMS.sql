DECLARE
    v_first_name VARCHAR2(25);
    v_last_name VARCHAR2(25);
BEGIN
    SELECT first_name, last_name
        INTO v_first_name, v_last_name
        FROM employees
        WHERE last_name = 'Oswald';
DBMS_OUTPUT.PUT_LINE ('The employee of the month is: '||v_first_name|| ' ' || v_last_name|| '.');
EXCEPTION
    WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE ('Your select statement retrieved multiple rows. Consider using a cursor or changing
        the search criteria.');
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No hay datos');
END;